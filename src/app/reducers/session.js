const INITIAL_STATE = {
  authUser: null,
  loadingUser: false
};

function sessionReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case 'AUTH_USER_SET':
      return {
        ...state,
        authUser: action.authUser,
        loadingUser: false
      };
    case 'AUTH_USER_CHECKING': {
      return {
        ...state,
        loadingUser: true
      };
    }
    default:
      return state;
  }
}

export default sessionReducer;
