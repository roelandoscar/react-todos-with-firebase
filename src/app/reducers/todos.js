// Reducer to handle all todo updates in the store

const INITIAL_STATE = {
  todos: {}
};

function todosReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case 'TODOS_LOADED':
      return {
        ...state,
        todos: action.todos
      };
    case 'TODO_ADDED':
      const todosAfterAdd = { ...state.todos };
      todosAfterAdd[action.id] = action.val;
      return {
        ...state,
        todos: todosAfterAdd
      };
    case 'TODO_REMOVED':
      const todosAfterRemove = { ...state.todos };
      delete todosAfterRemove[action.key];
      return {
        ...state,
        todos: todosAfterRemove
      };
    default:
      return state;
  }
}

export default todosReducer;
