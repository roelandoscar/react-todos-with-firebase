import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'recompose';

import CircularProgress from '@material-ui/core/CircularProgress';
import Grid from '@material-ui/core/Grid';

import withAuthentication from './components/Session';
import Authorize from './components/Authorize';
import Todos from './components/Todos';

import styles from './App.module.scss';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      authUser: null,
      loading: true
    };
  }

  render() {
    const user = !this.props.loadingUser && this.props.authUser;

    return (
      <Grid container className={styles.main} justify="center" align="center">
        {!this.props.loadingUser ? (
          <Grid item className={styles.container}>
            {user ? (
              <Todos
                user={user}
                todos={this.props.todosList}
                onLoadTodos={this.props.onLoadTodos}
                addTodo={this.props.addTodo}
                removeTodo={this.props.removeTodo}
              />
            ) : (
              <Authorize />
            )}
          </Grid>
        ) : (
          <CircularProgress />
        )}
      </Grid>
    );
  }
}

const mapStateToProps = state => {
  return {
    authUser: state.sessionState.authUser,
    loadingUser: state.sessionState.loadingUser,
    todosList: state.todosState.todos
  };
};

const mapDispatchToProps = dispatch => ({
  onLoadTodos: todos => dispatch({ type: 'TODOS_LOADED', todos }),
  addTodo: (id, val) => dispatch({ type: 'TODO_ADDED', id, val }),
  removeTodo: key => dispatch({ type: 'TODO_REMOVED', key })
});

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(withAuthentication(App));
