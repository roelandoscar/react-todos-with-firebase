// Component for the Todos view, shows after sign in
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import { TextField, Grid } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import SignoutIcon from '@material-ui/icons/ExitToApp';

import { db, auth } from '../../../services/firebase';
import TodosList from './TodosList';

import styles from './index.module.scss';

class Todos extends Component {
  constructor(props) {
    super(props);

    this.state = { newTodo: '' };
    this.addTodo = this.addTodo.bind(this);
    this.removeTodo = this.removeTodo.bind(this);
  }

  componentDidMount() {
    const uid = this.props.user.uid;
    db.getAllTodos(uid).then(todos => {
      const allTodos = todos.val() || {};
      this.props.onLoadTodos(allTodos);
    });
  }

  addTodo() {
    const uid = this.props.user.uid;
    this.setState({ newTodo: '' });

    db.createTodo(this.state.newTodo, Date.now(), uid).then(snap => {
      this.props.addTodo(snap.key, snap.val());
    });
  }

  removeTodo(key) {
    db.removeTodo(key)
      .then(r => {
        this.props.removeTodo(key);
      })
      .catch(e => console.log(e));
  }

  render() {
    return (
      <Card className={styles.card}>
        <Button
          color="secondary"
          classes={{ root: styles.signout }}
          onClick={() => {
            auth.singUserOut();
          }}>
          <SignoutIcon className={styles.signout_icon} />
          Sign out
        </Button>
        <h1>Your todos</h1>
        <hr className={styles.hr} />
        <TodosList {...this.props} removeTodo={this.removeTodo} />
        <hr />
        <Grid
          container
          spacing={8}
          alignItems="flex-end"
          className={styles.input_group}>
          <Grid item zeroMinWidth={true} style={{ flex: 1 }}>
            <TextField
              variant="filled"
              label="Add another todo"
              value={this.state.newTodo}
              onChange={event => this.setState({ newTodo: event.target.value })}
              style={{ width: '100%' }}
            />
          </Grid>
          <Grid item>
            <Button
              variant="fab"
              color="primary"
              aria-label="Add"
              onClick={this.addTodo}
              disabled={!this.state.newTodo}>
              <AddIcon />
            </Button>
          </Grid>
        </Grid>
      </Card>
    );
  }
}

export default Todos;
