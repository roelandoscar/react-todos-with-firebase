// The actual list component for all the todos of the user
import React from 'react';
import List from '@material-ui/core/List';
import ListItemText from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';

import styles from './index.module.scss';

const TodosList = ({ todos, removeTodo }) => {
  return (
    <List dense className={styles.list}>
      {Object.keys(todos).length ? (
        // Todos found, loop through the object
        Object.keys(todos).map((value, index) => (
          <ListItemText
            key={value}
            divider={index < Object.keys(todos).length - 1}
            primary={`${todos[value].title}`}>
            {todos[value].title}
            <ListItemSecondaryAction>
              <IconButton aria-label="Delete" onClick={() => removeTodo(value)}>
                <DeleteIcon />
              </IconButton>
            </ListItemSecondaryAction>
          </ListItemText>
        ))
      ) : (
        // If no todos found, show a default text
        <ListItemText key={`0`} primary={`Empty`}>
          <span>No todos yet! Create one below </span>
          <span role="img" aria-label="star-eyes" className={styles.emoji}>
            🤩
          </span>
        </ListItemText>
      )}
    </List>
  );
};

export default TodosList;
