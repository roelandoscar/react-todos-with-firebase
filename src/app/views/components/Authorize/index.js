// The Authorize component which the user will use to sign in/up to Firebase
import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';

import { auth } from '../../../services/firebase/index.js';
import styles from './index.module.scss';

const INITIAL_STATE = {
  email: '',
  emailError: false,
  password: '',
  passwordError: false,
  existing: null,
  error: null
};

class SignInForm extends Component {
  constructor(props) {
    super(props);

    this.state = { ...INITIAL_STATE };
  }

  onSubmit = event => {
    event.preventDefault();
    const { email, password } = this.state;

    // Try to sign the user in
    auth
      .signUserIn(email, password)
      .then(user => {
        // The user is signedin, reset the form, updat the redux store for the user
        this.setState(() => ({ ...INITIAL_STATE }));
        this.props.updateUser(user);
        this.setState({ error: { message: 'User found, signed in!' } });
      })
      .catch(error => {
        // Something went wrong with the sign in
        if (error.code === 'auth/user-not-found') {
          // The user doesn't exist yet, create the user
          this.setState({
            error: { message: 'User not found, creating account...' }
          });
          auth
            .createUser(email, password)
            .then(() => {
              console.log('created user in auth');
            })
            .catch(e => {
              this.setState({ error: error });
            });
        } else {
          // The user does exists but something else went wrong
          let passwordError = false;
          let emailError = false;
          switch (error.code) {
            case 'auth/wrong-password':
              // User used a wrong password
              passwordError = true;
              break;
            case 'auth/invalid-email':
              // User used a wrong email
              emailError = true;
              break;
            default:
              break;
          }
          this.setState({
            error,
            passwordError,
            emailError
          });
        }
      });
  };

  render() {
    const { email, password, error } = this.state;

    const isInvalid = password.length < 6 || email === '';

    return (
      <Card spacing={40} className={styles.card}>
        <form onSubmit={this.onSubmit}>
          <Grid
            container
            direction="column"
            justify="center"
            alignItems="center"
            spacing={32}
            className={styles.grid_container}>
            <Grid item style={{ width: '100%' }}>
              <TextField
                InputLabelProps={{ shrink: true }}
                placeholder={'email address'}
                fullWidth={true}
                error={this.state.emailError}
                onChange={event => this.setState({ email: event.target.value })}
              />
            </Grid>
            <Grid item style={{ width: '100%' }}>
              <TextField
                InputLabelProps={{ shrink: true }}
                placeholder={'password'}
                type="password"
                error={this.state.passwordError}
                fullWidth={true}
                onChange={event =>
                  this.setState({ password: event.target.value })
                }
              />
            </Grid>
            <Grid item>
              {error && (
                <Typography color="error">
                  {error.message}
                  <br />
                  <br />
                </Typography>
              )}
              <Button
                disabled={isInvalid}
                type="submit"
                color="primary"
                variant="contained">
                Sign in
              </Button>
            </Grid>
          </Grid>
          <Typography>
            If you're not signedup yet, we will sign you up with the password
            you fill in.
          </Typography>
        </form>
      </Card>
    );
  }
}

const Authorize = () => (
  <Grid container direction="column" justify="center" align="center">
    <h1>Please sign in to manage your todo's</h1>
    <SignInForm />
  </Grid>
);

export default Authorize;

export { SignInForm };
