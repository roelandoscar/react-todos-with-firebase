// Higher order component to check authentication of the user with Firebase
import React from 'react';
import { connect } from 'react-redux';

import { firebase, db } from '../../../services/firebase';

const withAuthentication = Component => {
  class WithAuthentication extends React.Component {
    componentDidMount() {
      const { onSetAuthUser, checkAuthUser } = this.props;
      checkAuthUser();
      firebase.auth.onAuthStateChanged(authUser => {
        authUser ? onSetAuthUser(authUser) : onSetAuthUser(null);
      });
    }

    render() {
      return <Component {...this.props} />;
    }
  }

  const mapDispatchToProps = (dispatch, store) => ({
    checkAuthUser: () => {
      // Function to enable us to show a loader
      dispatch({
        type: 'AUTH_USER_CHECKING'
      });
    },
    onSetAuthUser: authUser => {
      // Check if the users signup timestamp is the same as last login timestamp
      // if true, it is a new user, also create user in database
      if (authUser && authUser.metadata.a === authUser.metadata.b) {
        db.createUser(authUser.uid, authUser.email, authUser.email)
          .then(dbUser => {
            // Dispatch to redux after creation of user in database
            dispatch({
              type: 'AUTH_USER_SET',
              authUser,
              newUser: false
            });
          })
          .catch(e => {
            console.log('user not created', e);
          });
      } else {
        // Else just dispatch to redux
        dispatch({
          type: 'AUTH_USER_SET',
          authUser,
          newUser: authUser
            ? authUser.metadata.a === authUser.metadata.b
            : false
        });
      }
    }
  });

  return connect(
    null,
    mapDispatchToProps
  )(WithAuthentication);
};

export default withAuthentication;
