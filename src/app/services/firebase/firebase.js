// Initialize Firebase and load all config from env vars
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
import * as config from '../../../config';

if (!firebase.apps.length) {
  firebase.initializeApp(config);
}

const db = firebase.database();
const auth = firebase.auth();

export { auth, db };
