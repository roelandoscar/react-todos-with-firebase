// Calls to Firebase to handle authentication of the user
import { auth } from './firebase';

// Create the user
export const createUser = (email, password) =>
  auth.createUserWithEmailAndPassword(email, password);

// Sign the user in
export const signUserIn = (email, password) =>
  auth.signInWithEmailAndPassword(email, password);

// Sign the user out
export const singUserOut = () => auth.signOut();
