// Calls to the Firebase db to update users and todos
import { db } from './firebase';

// Create the user in the db
export const createUser = (id, username, email) =>
  db.ref(`users/${id}`).set({
    username,
    email
  });

// Get all todos which belong to the user
export const getAllTodos = uid =>
  db
    .ref('todos')
    .orderByChild('user')
    .equalTo(uid)
    .once('value');

// Create the new todo with all the data
export const createTodo = (title, timestamp, user) =>
  db
    .ref('todos')
    .push({
      title,
      timestamp,
      user
    })
    .once('value');

// Remove the todo from the database
export const removeTodo = key =>
  db
    .ref('todos')
    .child(key)
    .remove();
