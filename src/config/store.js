import { combineReducers, createStore } from 'redux';
import sessionReducer from '../app/reducers/session';
import todosReducer from '../app/reducers/todos';

const rootReducer = combineReducers({
  sessionState: sessionReducer,
  todosState: todosReducer
});
const store = createStore(rootReducer);

export default store;
